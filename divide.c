#include <stdio.h>

int divide(int a, int b);

int main(void)
{
    int c = 15, d = 5;
    printf("c / d = %d\n", divide(c, d));
}

int divide(int a, int b) {
    if (b)
        return a / b;
    exit(0);
}

